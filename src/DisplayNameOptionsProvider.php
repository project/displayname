<?php

namespace Drupal\displayname;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * The name option provider for the name field.
 */
class DisplayNameOptionsProvider {

  /**
   * The regular expression for finding the vocabulary token.
   *
   * @var string
   */
  const VOCABULARY_REGEXP = '/^\[vocabulary:([0-9a-z\_]{1,})\]/';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The term storage manager.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The vocabulary storage manager.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * Constructs a new NameOptionsProvider object.
   *
   * Initializes the entity type manager and module handler.
   * Load term and vocabulary storage if the taxonomy module exists.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;

    if ($this->entityTypeManager && $this->moduleHandler->moduleExists('taxonomy')) {
      $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
      $this->vocabularyStorage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
    }
  }

  /**
   * Options for a name component.
   *
   * Generates the list of options for a first name component.
   * If options include a vocabulary token, include terms from the vocabulary.
   * Duplicates are removed and options can be sorted if specified.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field
   *   The field definition.
   * @param string $component
   *   The name field component (e.g., 'first', 'last').
   *
   * @return array
   *   The list of options for the component.
   */
  public function getOptions(FieldDefinitionInterface $field, $component) {
    // Get field settings.
    $fs = $field->getSettings();
    // Get the options for the specified component.
    $options = $fs[$component . '_options'];

    // Iterate over options to check for vocabulary tokens.
    foreach ($options as $index => $opt) {
      if (preg_match(self::VOCABULARY_REGEXP, trim($opt), $matches)) {
        unset($options[$index]);
        // Load terms from the specified vocabulary if available.
        if ($this->termStorage && $this->vocabularyStorage) {
          $vocabulary = $this->vocabularyStorage->load($matches[1]);
          if ($vocabulary) {
            $max_length = $fs['max_length'][$component] ?? 255;
            foreach ($this->termStorage->loadTree($vocabulary->id()) as $term) {
              if (mb_strlen($term->name) <= $max_length) {
                $options[] = $term->name;
              }
            }
          }
        }
      }
    }

    // Remove duplicate options.
    $options = array_unique($options);

    // Sort options if specified.
    if (isset($fs['sort_options']) && !empty($fs['sort_options'][$component])) {
      natcasesort($options);
    }

    $default = FALSE;
    // Check for default options prefixed with '--'.
    foreach ($options as $index => $opt) {
      if (strpos($opt, '--') === 0) {
        unset($options[$index]);
        $default = trim(mb_substr($opt, 2));
      }
    }

    // Trim and combine options for the final list.
    $options = array_map('trim', $options);
    $options = array_combine($options, $options);

    // Add the default option if specified.
    if ($default !== FALSE) {
      $options = ['' => $default] + $options;
    }

    return $options;
  }

}
