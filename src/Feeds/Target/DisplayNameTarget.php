<?php

namespace Drupal\displayname\Feeds\Target;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\feeds\FieldTargetDefinition;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;

/**
 * Defines a display name field mapper.
 *
 * @FeedsTarget(
 *   id = "display_name",
 *   field_types = {
 *     "display_name"
 *   }
 * )
 */
class DisplayNameTarget extends FieldTargetBase {

  /**
   * Prepares the target definition for the display name field.
   *
   * This function creates a target definition based on the first name field
   * definition and adds properties for each component key of the display name.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition for which the target is being prepared.
   *
   * @return \Drupal\feeds\FieldTargetDefinition
   *   The prepared target definition with added properties for each
   *   display name component key.
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    // Create a target definition from the field definition.
    $target_definition = FieldTargetDefinition::createFromFieldDefinition($field_definition);

    // Iterate over each display name component key and
    // add it as a property to the target definition.
    foreach (_displayname_component_keys() as $key) {
      $target_definition->addProperty($key);
    }

    // Return the prepared target definition.
    return $target_definition;
  }

}
