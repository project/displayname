<?php

namespace Drupal\displayname\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Template\Attribute;

/**
 * Provides a display name render element.
 *
 * @RenderElement("display_name")
 */
class DisplayName extends FormElement implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRender'];
  }

  /**
   * Returns the element properties for this element.
   *
   * @return array
   *   An array of element properties.
   */
  public function getInfo() {
    $parts = _displayname_translations();
    $field_settings = \Drupal::service('plugin.manager.field.field_type')
      ->getDefaultFieldSettings('display_name');

    return [
      '#input' => TRUE,
      '#process' => ['displayname_element_expand'],
      '#pre_render' => [[__CLASS__, 'preRender']],
      '#element_validate' => ['displayname_element_validate'],
      '#theme_wrappers' => ['form_element'],
      '#show_component_required_marker' => 1,
      '#default_value' => [
        'title' => '',
        'first' => '',
        'middle' => '',
        'last' => '',
        'full' => '',
        'alias' => '',
      ],
      '#minimum_components' => $field_settings['minimum_components'],
      '#allow_last_or_first' => $field_settings['allow_last_or_first'],
      '#components' => [
        'title' => [
          'type' => $field_settings['field_type']['title'],
          'title' => $parts['title'],
          'title_display' => 'description',
          'size' => $field_settings['size']['title'],
          'maxlength' => $field_settings['max_length']['title'],
          'options' => $field_settings['title_options'],
          'autocomplete' => FALSE,
        ],
        'first' => [
          'type' => 'textfield',
          'title' => $parts['first'],
          'title_display' => 'description',
          'size' => $field_settings['size']['first'],
          'maxlength' => $field_settings['max_length']['first'],
          'autocomplete' => FALSE,
        ],
        'middle' => [
          'type' => 'textfield',
          'title' => $parts['middle'],
          'title_display' => 'description',
          'size' => $field_settings['size']['middle'],
          'maxlength' => $field_settings['max_length']['middle'],
          'autocomplete' => FALSE,
        ],
        'last' => [
          'type' => 'textfield',
          'title' => $parts['last'],
          'title_display' => 'description',
          'size' => $field_settings['size']['last'],
          'maxlength' => $field_settings['max_length']['last'],
          'autocomplete' => FALSE,
        ],
        'full' => [
          'type' => 'textfield',
          'title' => $parts['full'],
          'title_display' => 'description',
          'size' => $field_settings['size']['full'],
          'maxlength' => $field_settings['max_length']['full'],
          'autocomplete' => FALSE,
        ],
        'alias' => [
          'type' => 'textfield',
          'title' => $parts['alias'],
          'title_display' => 'description',
          'size' => $field_settings['size']['alias'],
          'maxlength' => $field_settings['max_length']['alias'],
          'autocomplete' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $value = [
      'title' => '',
      'first' => '',
      'middle' => '',
      'last' => '',
      'full' => '',
      'alias' => '',
    ];
    if ($input === FALSE) {
      $element += ['#default_value' => []];
      return $element['#default_value'] + $value;
    }
    // Throw out all invalid array keys; we only allow specific keys.
    foreach ($value as $allowed_key => $default) {
      if (isset($input[$allowed_key]) && is_scalar($input[$allowed_key])) {
        $value[$allowed_key] = (string) $input[$allowed_key];
      }
    }
    return $value;
  }

  /**
   * This function themes the element and controls the title display.
   *
   * @param array $element
   *   The render array of the element.
   *
   * @return array
   *   The altered element.
   */
  public static function preRender(array $element) {
    $layouts = displayname_widget_layouts();
    $layout = $layouts['stacked'];
    if (!empty($element['#widget_layout']) && isset($layouts[$element['#widget_layout']])) {
      $layout = $layouts[$element['#widget_layout']];
    }

    if (!empty($layout['library'])) {
      if (!isset($element['#attached']['library'])) {
        $element['#attached']['library'] = [];
      }
      $element['#attached']['library'] += $layout['library'];
    }

    $attributes = new Attribute($layout['wrapper_attributes']);
    $element['_displayname'] = [
      '#prefix' => '<div' . $attributes . '>',
      '#suffix' => '</div>',
    ];

    foreach (_displayname_translations() as $key => $title) {
      if (isset($element[$key])) {
        $element['_displayname'][$key] = $element[$key];
        unset($element[$key]);
      }
    }

    return $element;
  }

}
