<?php

namespace Drupal\displayname\Traits;

use Drupal\Core\Form\FormStateInterface;

/**
 * Display name form display settings trait.
 *
 * General form display settings.
 */
trait DisplayNameFormDisplaySettingsTrait {

  /**
   * Gets the default settings for controlling a name element.
   *
   * @return array
   *   Default settings.
   */
  protected static function getDefaultDisplayNameFormDisplaySettings() {
    return [
      'labels' => [
        'title' => t('Title'),
        'first' => t('First name'),
        'middle' => t('Middle name(s)'),
        'last' => t('Last name'),
        'full' => t('Full name'),
        'alias' => t('Nickname'),
      ],
      'size' => [
        'title' => 6,
        'first' => 20,
        'middle' => 20,
        'last' => 20,
        'full' => 35,
        'alias' => 35,
      ],
      'title_display' => [
        'title' => 'description',
        'first' => 'description',
        'middle' => 'description',
        'last' => 'description',
        'full' => 'description',
        'alias' => 'description',
      ],
      'widget_layout' => 'stacked',
      'field_title_display' => 'before',
      'show_component_required_marker' => TRUE,
    ];
  }

  /**
   * Returns a form for the default settings defined above.
   *
   * The following keys are closely tied to the pre-render function to theme
   * the settings into a nicer table.
   * - #indent_row: Adds an empty TD cell and adds an 'elements' child that
   *   contains the children (if first).
   * - #table_group: Used to either position within the table by the element
   *   key, or set to 'none', to append it below the table.
   *
   * Any element within the table should have component keyed children.
   *
   * Other elements are rendered directly.
   *
   * @param array $settings
   *   The settings.
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   * @param bool $has_data
   *   A flag to indicate if the field has data.
   *
   * @return array
   *   The form definition for the field settings.
   */
  protected function getDefaultDisplayNameFormDisplaySettingsForm(array $settings, array &$form, FormStateInterface $form_state, $has_data = TRUE) {
    $components = _displayname_translations();

    $title_display_options = [
      'title' => $this->t('above'),
      'description' => $this->t('below'),
      'placeholder' => $this->t('placeholder'),
      'attribute' => $this->t('attribute'),
      'none' => $this->t('hidden'),
    ];

    $element = [];

    // Placeholder for additional fields to couple with the components section.
    $element['components_extra'] = [
      '#indent_row' => TRUE,
    ];

    // Show component required marker setting.
    $element['show_component_required_marker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show component required marker'),
      '#default_value' => $this->getSetting('show_component_required_marker'),
      '#description' => $this->t('Appends an asterisk after the component title if the component is required as part of a complete name.'),
      '#table_group' => 'components_extra',
    ];

    // Labels setting.
    $element['labels'] = [
      '#title' => $this->t('Labels'),
      '#description' => $this->t('The labels are used to distinguish the components.'),
    ];

    // Title display setting.
    $element['title_display'] = [
      '#title' => $this->t('Label display'),
      '#description' => $this->t('The title display controls how the label of the name component is displayed in the form:<br>"%above" is the standard title;<br>"%below" is the standard description;<br>"%placeholder" uses the placeholder attribute, select lists do not support this option;<br>"%attribute" adds a title attribute to create a tooltip rather than a label.<br>"%hidden" removes the label.', [
        '%above' => $this->t('above'),
        '%below' => $this->t('below'),
        '%placeholder' => $this->t('placeholder'),
        '%attribute' => $this->t('attribute'),
        '%hidden' => $this->t('hidden'),
      ]),
    ];

    // HTML size setting.
    $element['size'] = [
      '#title' => $this->t('HTML size'),
      '#description' => $this->t('The HTML size property tells the browser what the width of the field should be when it is rendered. This gets overridden by the themes CSS properties. This must be between 1 and 255.'),
    ];

    foreach ($components as $key => $title) {
      // Labels for components.
      $element['labels'][$key] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label for @title', ['@title' => $title]),
        '#title_display' => 'invisible',
        '#default_value' => $settings['labels'][$key],
        '#required' => TRUE,
        '#size' => 10,
      ];
      // HTML size for components.
      $element['size'][$key] = [
        '#type' => 'number',
        '#min' => 1,
        '#max' => 255,
        '#title' => $this->t('HTML size property for @title', ['@title' => $title]),
        '#title_display' => 'invisible',
        '#default_value' => $settings['size'][$key],
        '#required' => FALSE,
        '#size' => 10,
      ];
      // Title display for components.
      $element['title_display'][$key] = [
        '#type' => 'radios',
        '#title' => $this->t('Label display for @title', ['@title' => $title]),
        '#title_display' => 'invisible',
        '#default_value' => $settings['title_display'][$key],
        '#options' => $title_display_options,
      ];
    }

    // Field title display setting.
    $element['field_title_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Title Display'),
      '#description' => $this->t('Whether to show or hide the top-level field label.'),
      '#default_value' => $this->getSetting('field_title_display'),
      '#table_group' => 'above',
      '#options' => [
        'before' => $this->t('Before'),
        'invisible' => $this->t('Visually hidden'),
        'none' => $this->t('Hidden'),
      ],
      '#weight' => -49,
    ];

    // Widget layout setting.
    $widget_layout_options = [];
    foreach (displayname_widget_layouts() as $layout => $info) {
      $widget_layout_options[$layout] = $info['label'];
    }
    $element['widget_layout'] = [
      '#type' => 'radios',
      '#title' => $this->t('Widget layout'),
      '#default_value' => $this->getSetting('widget_layout'),
      '#options' => $widget_layout_options,
      '#table_group' => 'above',
      '#required' => TRUE,
      '#weight' => -50,
    ];

    return $element;
  }

}
