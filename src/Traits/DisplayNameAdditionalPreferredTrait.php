<?php

namespace Drupal\displayname\Traits;

use Drupal\Core\Form\FormStateInterface;

/**
 * Display name form for preferred and alternative settings trait.
 */
trait DisplayNameAdditionalPreferredTrait {

  /**
   * Gets the default settings for alternative and preferred fields.
   *
   * @return array
   *   Default settings.
   */
  protected static function getDefaultAdditionalPreferredSettings() {
    return [
      "preferred_field_reference" => "",
      "preferred_field_reference_separator" => ", ",
      "alternative_field_reference" => "",
      "alternative_field_reference_separator" => ", ",
    ];
  }

  /**
   * Returns a form for the default settings defined above.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   *
   * @return array
   *   The form definition for the field settings.
   */
  protected function getNameAdditionalPreferredSettingsForm(array &$form, FormStateInterface $form_state) {
    $elements = [];

    // Preferred component source.
    $elements['preferred_field_reference'] = [
      '#type' => 'select',
      '#title' => $this->t('Preferred component source'),
      '#default_value' => $this->getSetting('preferred_field_reference'),
      '#empty_option' => $this->getEmptyOption(),
      '#options' => $this->getAdditionalSources(),
      '#description' => $this->t('A data source for the preferred first name within the name formats, commonly used for a user’s alias.<br>e.g., "q" and "v", plus the conditional "p", "d", and "D" name format options.'),
    ];

    // Preferred component source multivalued separator.
    $elements['preferred_field_reference_separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Preferred source multivalued separator'),
      '#default_value' => $this->getSetting('preferred_field_reference_separator'),
      '#description' => $this->t('Separator for multi-value items in an inline list.'),
      '#states' => [
        'invisible' => [
          ':input[name$="[preferred_field_reference]"]' => ['value' => ''],
        ],
      ],
    ];

    // Alternative component source.
    $elements['alternative_field_reference'] = [
      '#type' => 'select',
      '#title' => $this->t('Alternative component source'),
      '#default_value' => $this->getSetting('alternative_field_reference'),
      '#empty_option' => $this->getEmptyOption(),
      '#options' => $this->getAdditionalSources(),
      '#description' => $this->t('A data source for the alternative component within the name formats, useful for custom formatted names for citations or user credentials/post-nominal letters.<br>e.g., "a" and "A" name format options.'),
    ];

    // Alternative component source multivalued separator.
    $elements['alternative_field_reference_separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alternative source multivalued separator'),
      '#default_value' => $this->getSetting('alternative_field_reference_separator'),
      '#description' => $this->t('Separator for multi-value items in an inline list.'),
      '#states' => [
        'invisible' => [
          ':input[name$="[alternative_field_reference]"]' => ['value' => ''],
        ],
      ],
    ];

    return $elements;
  }

  /**
   * Adds preferred and alternative field settings to the summary array.
   *
   * @param array $summary
   *   The summary array to add to.
   */
  protected function settingsNameAdditionalPreferredSummary(array &$summary) {
    // Add the preferred field reference summary.
    if ($type = $this->getSetting('preferred_field_reference')) {
      $targets = $this->getAdditionalSources();
      $summary[] = $this->t('Preferred: @label', [
        '@label' => empty($targets[$type]) ? $this->t('-- invalid --') : $targets[$type],
      ]);
    }
    elseif (!$this->getTraitUsageIsField()) {
      if ($type = $this->fieldDefinition->getSetting('preferred_field_reference')) {
        $targets = $this->getAdditionalSources();
        $summary[] = $this->t('Preferred: field default (@label)', [
          '@label' => empty($targets[$type]) ? $this->t('-- invalid --') : $targets[$type],
        ]);
      }
      else {
        $summary[] = $this->t('Preferred: field default (-- none --)');
      }
    }

    // Add the alternative field reference summary.
    if ($type = $this->getSetting('alternative_field_reference')) {
      $targets = $this->getAdditionalSources();
      $summary[] = $this->t('Alternative: @label', [
        '@label' => empty($targets[$type]) ? $this->t('-- invalid --') : $targets[$type],
      ]);
    }
    elseif (!$this->getTraitUsageIsField()) {
      if ($type = $this->fieldDefinition->getSetting('alternative_field_reference')) {
        $targets = $this->getAdditionalSources();
        $summary[] = $this->t('Alternative: field default (@label)', [
          '@label' => empty($targets[$type]) ? $this->t('-- invalid --') : $targets[$type],
        ]);
      }
      else {
        $summary[] = $this->t('Alternative: field default (-- none --)');
      }
    }
  }

  /**
   * Helper function to find attached fields to use as alternative sources.
   *
   * @return array
   *   The discovered additional sources.
   */
  protected function getAdditionalSources() {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }
    if (!isset($this->entityFieldManager)) {
      $this->entityFieldManager = \Drupal::service('entity_field.manager');
    }

    // Retrieve the field definition.
    $field_definition = empty($this->fieldDefinition) ? $this->getFieldDefinition() : $this->fieldDefinition;
    $entity_type_id = $field_definition->getTargetEntityTypeId();
    $entity_type = $this->entityTypeManager->getStorage($entity_type_id)->getEntityType();
    $bundle = $field_definition->getTargetBundle();
    $entity_type_label = $entity_type->getBundleLabel() ?: $entity_type->getLabel();

    // Initialize the sources array.
    $sources = [
      '_self' => $this->t('@label label', ['@label' => $entity_type_label]),
    ];

    // Add specific user entity source.
    if ($entity_type_id == 'user') {
      $sources['_self_property_name'] = $this->t('@label login name', ['@label' => $entity_type_label]);
    }

    // Add field definitions to the sources.
    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);
    foreach ($fields as $field_name => $field) {
      if (!$field->getFieldStorageDefinition()->isBaseField() && $field_name != $field_definition->getName()) {
        $sources[$field->getName()] = $field->getLabel();
      }
    }

    return $sources;
  }

  /**
   * Helper function to get the empty option text.
   *
   * @return string
   *   The empty option text.
   */
  protected function getEmptyOption() {
    return $this->getTraitUsageIsField() ? $this->t('-- none --') : $this->t('-- field default --');
  }

  /**
   * Determines if the trait is used in a field.
   *
   * @return bool
   *   TRUE if the trait is used in a field, FALSE otherwise.
   */
  protected function getTraitUsageIsField() {
    return is_subclass_of($this, 'Drupal\Core\Field\FieldItemBase');
  }

}
