<?php

namespace Drupal\displayname\Traits;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\displayname\DisplayNameOptionsProvider;

/**
 * Display name settings trait.
 *
 * Shared methods to assist handling the field element setting forms.
 */
trait DisplayNameFormSettingsHelperTrait {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['fieldSettingsFormPreRender'];
  }

  /**
   * Themes up the field settings into a table.
   *
   * @param array $form
   *   The form to be pre-rendered.
   *
   * @return array
   *   The themed form.
   */
  public function fieldSettingsFormPreRender(array $form) {
    $components = _displayname_translations();
    $excluded_components = $form['#excluded_components'] ?? [];

    // Setup the base structure for the field settings form.
    $form = [
      'top' => [],
      'hidden' => ['#access' => FALSE],
      'name_settings' => [
        '#type' => 'container',
        'table' => [
          '#prefix' => '<table>',
          '#suffix' => '</table>',
          '#weight' => -2,
          'thead' => [
            '#prefix' => '<thead><tr><th>' . t('Field') . '</th>',
            '#suffix' => '</tr></thead>',
            '#weight' => -3,
          ],
          'tbody' => [
            '#prefix' => '<tbody>',
            '#suffix' => '</tbody>',
            '#weight' => -2,
          ],
        ],
      ] + ($form['name_settings'] ?? []),
    ] + $form;

    // Add headers for each component.
    foreach ($components as $key => $title) {
      if (!empty($excluded_components[$key])) {
        continue;
      }
      $form['name_settings']['table']['thead'][$key] = [
        '#markup' => $title,
        '#prefix' => '<th>',
        '#suffix' => '</th>',
      ];
    }

    $help_footer_notes = [];
    $footer_notes_counter = 0;

    // Process each child element in the form.
    foreach (Element::children($form) as $child) {
      if (in_array($child, ['name_settings', 'top', 'hidden'])) {
        continue;
      }

      if (!empty($form[$child]['#table_group'])) {
        if ($form[$child]['#table_group'] == 'none') {
          continue;
        }
        if ($form[$child]['#table_group'] == 'above') {
          $form['top'][$child] = $form[$child];
          unset($form[$child]);
        }
        else {
          $target_key = $form[$child]['#table_group'];
          $form['name_settings']['table']['tbody'][$target_key]['elements'][$child] = $form[$child];
          unset($form[$child]);
        }
      }
      elseif (!empty($form[$child]['#indent_row'])) {
        $form['name_settings']['table']['tbody'][$child] = [
          '#prefix' => '<tr><td>&nbsp;</td>',
          '#suffix' => '</tr>',
          'elements' => [
            '#prefix' => '<td colspan="' . (6 - count($excluded_components)) . '">',
            '#suffix' => '</td>',
          ] + $form[$child],
        ];
        unset($form[$child]);
      }
      else {
        $footnote_sup = '';
        if (!empty($form[$child]['#description'])) {
          $footnote_sup = $this->t('<sup>@number</sup>', ['@number' => ++$footer_notes_counter]);
          $help_footer_notes[] = $form[$child]['#description'];
          unset($form[$child]['#description']);
        }
        if (isset($form[$child]['#title'])) {
          $form['name_settings']['table']['tbody'][$child] = [
            '#prefix' => '<tr><th>' . $form[$child]['#title'] . $footnote_sup . '</th>',
            '#suffix' => '</tr>',
          ];
        }
        foreach (array_keys($components) as $weight => $key) {
          if (!empty($excluded_components[$key]) && isset($form[$child][$key])) {
            $form[$child][$key]['#access'] = FALSE;
            $form['hidden'][$child][$key] = $form[$child][$key];
          }
          else {
            if (isset($form[$child][$key])) {
              $form[$child][$key]['#attributes']['title'] = $form[$child][$key]['#title'];
              if (isset($form[$child][$key]['#type']) && $form[$child][$key]['#type'] === 'checkbox') {
                $form[$child][$key]['#title_display'] = 'invisible';
              }
              $form['name_settings']['table']['tbody'][$child][$key] = [
                '#prefix' => '<td>',
                '#suffix' => '</td>',
                '#weight' => $weight,
              ] + $form[$child][$key];
              if ($child != 'components') {
                $form['name_settings']['table']['tbody'][$child][$key]['#states'] = [
                  'visible' => [
                    ':input[name$="[components][' . $key . ']"]' => ['checked' => TRUE],
                  ],
                ];
              }
            }
            else {
              $form['name_settings']['table']['tbody'][$child][$key] = [
                '#prefix' => '<td>',
                '#suffix' => '</td>',
                '#markup' => "&nbsp;",
                '#weight' => $weight,
              ];
            }
          }
        }
        unset($form[$child]);
      }
    }

    if ($help_footer_notes) {
      $form['name_settings']['footnotes'] = [
        '#type' => 'details',
        '#title' => t('Footnotes'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#parents' => [],
        '#weight' => -1,
        'help_items' => [
          '#theme' => 'item_list',
          '#list_type' => 'ol',
          '#items' => $help_footer_notes,
        ],
      ];
    }

    $form['#sorted'] = FALSE;

    return $form;
  }

  /**
   * Helper function to validate options.
   *
   * This function validates the allowed values for a field element.
   * It checks if the values are valid according to the provided constraints
   * such as maximum length, and also verifies if the taxonomy module is enabled
   * when required. It sets appropriate errors in the form state if any
   * validation fails.
   *
   * @param array $element
   *   Element being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param mixed $values
   *   Values to check.
   * @param int $max_length
   *   The max length allowed for the values.
   */
  protected static function validateOptions(array $element, FormStateInterface $form_state, $values, int $max_length) {
    // Extract the label for the element for use in error messages.
    $label = $element['#title'];

    // Initialize arrays to store different types of options for validation.
    $long_options = [];
    $valid_options = [];
    $default_options = [];

    // Loop through each value to validate it.
    foreach ($values as $value) {
      // Trim whitespace from the value.
      $value = trim($value);

      // Check if the value is a blank option (starts with '--').
      if (strpos($value, '--') === 0) {
        $default_options[] = $value;
      }
      // Check if the value is a valid taxonomy term reference.
      elseif (preg_match(DisplayNameOptionsProvider::VOCABULARY_REGEXP, $value, $matches)) {
        // Ensure the taxonomy module is enabled.
        if (!\Drupal::moduleHandler()->moduleExists('taxonomy')) {
          // Set an error if the taxonomy module is not enabled.
          $form_state->setError($element, t("The taxonomy module must be enabled before using the '%tag' tag in %label.", [
            '%tag' => $matches[0],
            '%label' => $label,
          ]));
        }
        // Ensure the taxonomy reference is on a line by itself.
        elseif ($value !== $matches[0]) {
          // Set an error if the taxonomy reference is not on a line by itself.
          $form_state->setError($element, t("The '%tag' tag in %label should be on a line by itself.", [
            '%tag' => $matches[0],
            '%label' => $label,
          ]));
        }
        else {
          // Verify if the referenced vocabulary exists.
          $vocabulary = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary')->load($matches[1]);
          if ($vocabulary) {
            $valid_options[] = $value;
          }
          else {
            // Set an error if the vocabulary does not exist.
            $form_state->setError($element, t("The vocabulary '%tag' in %label could not be found.", [
              '%tag' => $matches[1],
              '%label' => $label,
            ]));
          }
        }
      }
      // Check if the value exceeds the maximum allowed length.
      elseif (mb_strlen($value) > $max_length) {
        // Add the value to the list of options that are too long.
        $long_options[] = $value;
      }
      // Check if the value is not empty.
      elseif (!empty($value)) {
        // Add the value to the list of valid options.
        $valid_options[] = $value;
      }
    }

    // Set an error if there are any options that exceed the maximum length.
    if (count($long_options)) {
      $form_state->setError($element, t('The following options exceed the maximum allowed %label length: %options', [
        '%options' => implode(', ', $long_options),
        '%label' => $label,
      ]));
    }
    // Set an error if there are no valid options.
    elseif (empty($valid_options)) {
      $form_state->setError($element, t('%label are required.', [
        '%label' => $label,
      ]));
    }
    // Set an error if there is more than one default (blank) option.
    elseif (count($default_options) > 1) {
      $form_state->setError($element, t('%label can only have one blank value assigned to it.', [
        '%label' => $label,
      ]));
    }

    // Merge the default options and valid options,
    // and set the value for the element.
    $form_state->setValueForElement($element, array_merge($default_options, $valid_options));
  }

  /**
   * Helper function to get the allowed values.
   *
   * @param string $string
   *   The string to parse.
   *
   * @return array
   *   The parsed values.
   */
  protected static function extractAllowedValues($string) {
    return array_filter(array_map('trim', explode("\n", $string)));
  }

}
