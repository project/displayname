<?php

namespace Drupal\displayname\Traits;

use Drupal\Core\Form\FormStateInterface;

/**
 * Display name settings trait.
 *
 * Used for handling the core field settings.
 */
trait DisplayNameFieldSettingsTrait {

  /**
   * Gets the default settings for controlling a name element.
   *
   * @return array
   *   Default settings.
   */
  protected static function getDefaultDisplayNameFieldSettings() {
    return [
      'components' => [
        'title' => FALSE,
        'first' => TRUE,
        'middle' => FALSE,
        'last' => TRUE,
        'full' => FALSE,
        'alias' => FALSE,
      ],
      'minimum_components' => [
        'title' => FALSE,
        'first' => TRUE,
        'middle' => FALSE,
        'last' => FALSE,
        'full' => FALSE,
        'alias' => FALSE,
      ],
      'allow_last_or_first' => FALSE,
      'max_length' => [
        'title' => 31,
        'first' => 63,
        'middle' => 127,
        'last' => 63,
        'full' => 255,
        'alias' => 255,
      ],
      'field_type' => [
        'title' => 'select',
        'first' => 'text',
        'middle' => 'text',
        'last' => 'text',
        'full' => 'text',
        'alias' => 'text',
      ],
      'autocomplete_source' => [
        'title' => [
          'title',
        ],
        'first' => [],
        'middle' => [],
        'last' => [],
        'full' => [],
        'alias' => [],
      ],
      'autocomplete_separator' => [
        'title' => ' ',
        'first' => ' -',
        'middle' => ' -',
        'last' => ' -',
        'full' => ' ',
        'alias' => ', ',
      ],
      'title_options' => [
        t('-- --'),
        t('Mr.'),
        t('Mrs.'),
        t('Miss'),
        t('Ms.'),
        t('Dr.'),
        t('Prof.'),
      ],
      'sort_options' => [
        'title' => FALSE,
        'first' => FALSE,
        'middle' => FALSE,
        'last' => FALSE,
        'full' => FALSE,
        'alias' => FALSE,
      ],
    ];
  }

  /**
   * Returns a form for the default settings defined above.
   *
   * @param array $settings
   *   The settings.
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   * @param bool $has_data
   *   A flag to indicate if the field has data.
   *
   * @return array
   *   The form definition for the field settings.
   */
  protected function getDefaultDisplayNameFieldSettingsForm(array $settings, array &$form, FormStateInterface $form_state, $has_data = TRUE) {
    $components = _displayname_translations();
    $field_options = [
      'select' => $this->t('Drop-down'),
      'text' => $this->t('Text field'),
      'autocomplete' => $this->t('Autocomplete'),
    ];

    $autocomplete_sources_options = [
      'title' => $this->t('Title options'),
      'full' => $this->t('Generational options'),
    ];

    $element = [];

    // Components settings.
    $element['components'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Components'),
      '#default_value' => array_keys(array_filter($settings['components'])),
      '#required' => TRUE,
      '#description' => $this->t('Only selected components will be activated on this field. All non-selected components/component settings will be ignored.'),
      '#options' => $components,
    ];

    // Minimum components settings.
    $element['minimum_components'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Minimum components'),
      '#default_value' => array_keys(array_filter($settings['minimum_components'])),
      '#required' => TRUE,
      '#description' => $this->t('The minimal set of components required before the field is considered completed enough to save.'),
      '#options' => $components,
      '#element_validate' => [[get_class($this), 'validateMinimumComponents']],
    ];

    // Additional components settings.
    $element['components_extra'] = [
      '#indent_row' => TRUE,
    ];
    $element['allow_last_or_first'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow a single valid first or last name value to fulfill the minimum component requirements for both first and last components.'),
      '#default_value' => !empty($settings['allow_last_or_first']),
      '#table_group' => 'components_extra',
    ];

    // Field type settings.
    $element['field_type'] = [
      '#title' => $this->t('Field type'),
      '#description' => $this->t('The Field type controls how the field is rendered. Autocomplete is a text field with autocomplete, and the behavior of this is controlled by the field settings.'),
    ];

    // Maximum length settings.
    $element['max_length'] = [
      '#title' => $this->t('Maximum length'),
      '#description' => $this->t('The maximum length of the field in characters. This must be between 1 and 255.'),
    ];

    // Sort options settings.
    $sort_options = is_array($settings['sort_options']) ? $settings['sort_options'] : [
      'title' => 'title',
      'full' => '',
    ];
    $element['sort_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Sort options'),
      '#default_value' => $sort_options,
      '#description' => $this->t("This enables sorting on the options after the vocabulary terms are added and duplicate values are removed."),
      '#options' => _displayname_translations([
        'title' => '',
        'full' => '',
      ]),
    ];

    // Autocomplete source settings.
    $element['autocomplete_source'] = [
      '#title' => $this->t('Autocomplete sources'),
      '#description' => $this->t('At least one value must be selected before you can enable the autocomplete option on the input text fields.'),
    ];

    // Autocomplete separator settings.
    $element['autocomplete_separator'] = [
      '#title' => $this->t('Autocomplete separator'),
      '#description' => $this->t('This allows you to override the default handling that the autocomplete uses to handle separations between components. If empty, this defaults to a single space.'),
    ];

    foreach ($components as $key => $title) {
      $min_length = 1;
      $element['max_length'][$key] = [
        '#type' => 'number',
        '#min' => $min_length,
        '#max' => 255,
        '#title' => $this->t('Maximum length for @title', ['@title' => $title]),
        '#title_display' => 'invisible',
        '#default_value' => $settings['max_length'][$key],
        '#required' => TRUE,
        '#size' => 5,
      ];
      $element['autocomplete_source'][$key] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Autocomplete options'),
        '#title_display' => 'invisible',
        '#default_value' => $settings['autocomplete_source'][$key],
        '#options' => $autocomplete_sources_options,
      ];
      if ($key != 'title') {
        unset($element['autocomplete_source'][$key]['#options']['title']);
      }
      $element['autocomplete_separator'][$key] = [
        '#type' => 'textfield',
        '#title' => $this->t('Autocomplete separator for @title', ['@title' => $title]),
        '#title_display' => 'invisible',
        '#default_value' => $settings['autocomplete_separator'][$key],
        '#size' => 10,
      ];
      $element['field_type'][$key] = [
        '#type' => 'radios',
        '#title' => $this->t('@title field type', ['@title' => $components['title']]),
        '#title_display' => 'invisible',
        '#default_value' => $settings['field_type'][$key],
        '#required' => TRUE,
        '#options' => $field_options,
      ];

      if ($key != 'title') {
        unset($element['field_type'][$key]['#options']['select']);
      }
    }

    // Title options settings.
    $title_options = implode("\n", array_filter($settings['title_options']));
    $element['title_options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('@title options', ['@title' => $components['title']]),
      '#default_value' => $title_options,
      '#required' => TRUE,
      '#description' => $this->t("Enter one @title per line. Prefix a line using '--' to specify a blank value text. For example: '--Please select a @title'.", [
        '@title' => $components['title'],
      ]),
      '#element_validate' => [[get_class($this), 'validateTitleOptions']],
      '#table_group' => 'none',
    ];

    return $element;
  }

  /**
   * Helper function to validate minimum components.
   *
   * @param array $element
   *   Element being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateMinimumComponents(array $element, FormStateInterface $form_state) {
    // Validate that at least one of 'first' or 'last'
    // is selected as a minimum component.
    $components_array = [
      'first',
      'last',
    ];
    $settings_array = [
      'settings',
      'minimum_components',
    ];
    $minimum_components = $form_state->getValue($settings_array);
    $diff = array_intersect(array_keys(array_filter($minimum_components)), $components_array);
    if (count($diff) == 0) {
      $components = array_intersect_key(_displayname_translations(), array_flip($components_array));
      $form_state->setError($element, t('%label must have one of the following components: %components', [
        '%label' => t('Minimum components'),
        '%components' => implode(', ', $components),
      ]));
    }

    // Ensure selected minimum components are part of the selected components.
    $components = $form_state->getValue([
      'settings',
      'components',
    ]);
    $minimum_components = $form_state->getValue($settings_array);
    $diff = array_diff_key(array_filter($minimum_components), array_filter($components));
    if (count($diff)) {
      $components = array_intersect_key(_displayname_translations(), $diff);
      $form_state->setError($element, t('%components cannot be selected for %label when they are not selected for %label2.', [
        '%label' => t('Minimum components'),
        '%label2' => t('Components'),
        '%components' => implode(', ', $components),
      ]));
    }
  }

  /**
   * Helper function to validate title options.
   *
   * @param array $element
   *   Element being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateTitleOptions(array $element, FormStateInterface $form_state) {
    $values = static::extractAllowedValues($element['#value']);
    $max_length = $form_state->getValue(['settings', 'max_length', 'title']);
    static::validateOptions($element, $form_state, $values, $max_length);
  }

  /**
   * Helper function to validate generational options.
   *
   * @param array $element
   *   Element being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateGenerationalOptions(array $element, FormStateInterface $form_state) {
    $values = static::extractAllowedValues($element['#value']);
    $max_length_keys = [
      'settings',
      'max_length',
      'full',
    ];
    $max_length = $form_state->getValue($max_length_keys);
    static::validateOptions($element, $form_state, $values, $max_length);
  }

}
