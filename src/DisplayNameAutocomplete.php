<?php

namespace Drupal\displayname;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Defines a helper class to get display name field autocompletion results.
 */
class DisplayNameAutocomplete {

  /**
   * Display name options provider.
   *
   * @var DisplayNameOptionsProvider
   */
  protected $optionsProvider;

  /**
   * Display name field components.
   *
   * @var array
   */
  protected $allComponents = [
    'first',
    'middle',
    'last',
    'title',
    'alias',
    'full',
  ];

  /**
   * Constructor for the DisplayNameAutocomplete class.
   *
   * @param DisplayNameOptionsProvider $options_provider
   *   Display name options provider.
   */
  public function __construct(DisplayNameOptionsProvider $options_provider) {
    $this->optionsProvider = $options_provider;
  }

  /**
   * Get matches for the autocompletion of display name components.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field
   *   The field definition.
   * @param string $target
   *   The name field component.
   * @param string $string
   *   The string to match for the display name field component.
   *
   * @return array
   *   An array containing the matching values.
   */
  public function getMatches(FieldDefinitionInterface $field, $target, $string) {
    $matches = [];
    // Limit the number of autocomplete results.
    $limit = 10;

    // Return empty matches if the input string is empty.
    if (empty($string)) {
      return $matches;
    }

    $settings = $field->getSettings();
    foreach ($this->allComponents as $component) {
      if (!isset($settings['autocomplete_source'][$component])) {
        $settings['autocomplete_source'][$component] = [];
      }
      $settings['autocomplete_source'][$component] = array_filter($settings['autocomplete_source'][$component]);
    }

    $action = [];
    // Determine the components to be searched based on the target.
    switch ($target) {
      case 'name':
        $components = [
          'first',
          'middle',
          'last',
        ];
        $action['components'] = $this->mapAssoc($components);
        break;

      case 'name-all':
        $action['components'] = $this->mapAssoc($this->allComponents);
        break;

      case 'title':
      case 'first':
      case 'middle':
      case 'last':
      case 'full':
      case 'alias':
        $action['components'] = [$target => $target];
        break;

      default:
        $action['components'] = [];
        foreach (explode('-', $target) as $component) {
          if (in_array($component, _displayname_component_keys())) {
            $action['components'][$component] = $component;
          }
        }
        break;
    }

    // Initialize the action source array for different components.
    $action['source'] = [
      'title' => [],
      'full' => [],
    ];

    // Initialize the separator used for splitting the string.
    $action['separator'] = '';

    foreach ($action['components'] as $component) {
      if (empty($settings['autocomplete_source'][$component])) {
        unset($action['components'][$component]);
      }
      else {
        $sep = (string) $settings['autocomplete_separator'][$component];
        if (strlen($sep) === 0) {
          $sep = ' ';
        }
        for ($i = 0; $i < strlen($sep); $i++) {
          if (strpos($action['separator'], $sep[$i]) === FALSE) {
            $action['separator'] .= $sep[$i];
          }
        }
        $found_source = FALSE;

        // Determine the source for autocomplete data.
        foreach ((array) $settings['autocomplete_source'][$component] as $src) {
          if ($src == 'data' && !$field) {
            continue;
          }
          if ($src == 'title' || $src == 'full') {
            if (!$field || $component != $src) {
              continue;
            }
          }
          $found_source = TRUE;
          $action['source'][$src][] = $component;
        }

        if (!$found_source) {
          unset($action['components'][$component]);
        }
      }
    }

    // Split the input string into individual components based on
    // the defined separator. Special handling is applied when the
    // separator is a single space (' '), utilizing explode()
    // instead of preg_split() for accurate splitting.
    if ($action['separator'] === ' ') {
      $pieces = explode(' ', $string);
    }
    else {
      $pieces = preg_split('/[' . preg_quote($action['separator']) . ']+/', $string);
    }

    // Process each piece of the input string to find matches.
    if (!empty($pieces) && !empty($action['components'])) {
      $test_string = mb_strtolower(array_pop($pieces));
      $base_string = mb_substr($string, 0, mb_strlen($string) - mb_strlen($test_string));

      // Search in the title options for matches.
      if ($limit > 0 && count($action['source']['title'])) {
        $options = $this->optionsProvider->getOptions($field, 'title');
        foreach ($options as $key => $option) {
          if (strpos(mb_strtolower($key), $test_string) === 0 || strpos(mb_strtolower($option), $test_string) === 0) {
            $matches[$base_string . $key] = $key;
            $limit--;
            if ($limit <= 0) {
              break;
            }
          }
        }
      }

      // Search in the full name options for matches.
      if ($limit > 0 && count($action['source']['full'])) {
        $options = $this->optionsProvider->getOptions($field, 'full');
        foreach ($options as $key => $option) {
          if (strpos(mb_strtolower($key), $test_string) === 0 || strpos(mb_strtolower($option), $test_string) === 0) {
            $matches[$base_string . $key] = $key;
            $limit--;
            if ($limit <= 0) {
              break;
            }
          }
        }
      }
    }

    return $matches;
  }

  /**
   * Helper function to combine values.
   *
   * @param array $values
   *   Array values to combine.
   *
   * @return array
   *   Combined array values.
   */
  public function mapAssoc(array $values) {
    return array_combine($values, $values);
  }

}
