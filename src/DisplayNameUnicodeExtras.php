<?php

namespace Drupal\displayname;

use Drupal\Component\Utility\Unicode;

/**
 * Provides custom Unicode-related extension methods.
 *
 * @ingroup utility
 */
class DisplayNameUnicodeExtras extends Unicode {

  /**
   * Split each word in a UTF-8 string.
   *
   * This function splits the first name text into an array of words based on
   * word boundaries defined by a regular expression.
   *
   * @param string $text
   *   The text that will be converted.
   *
   * @return array
   *   The input $text as an array of words.
   */
  public static function explode($text) {
    // Define a regular expression to match word boundaries.
    $regex = '/(^|[' . static::PREG_CLASS_WORD_BOUNDARY . '])/u';
    // Use preg_split to split the text into an array of words.
    $words = preg_split($regex, $text, -1, PREG_SPLIT_NO_EMPTY);
    // Return the array of words.
    return $words;
  }

  /**
   * Generate the initials of all first characters in a string.
   *
   * This function converts the first name text to lowercase, then generates
   * the initials of each word, and finally returns the initials
   * in uppercase, separated by the specified delimiter.
   *
   * Note that this is case-insensitive, camel case words are treated as a
   * single word.
   *
   * @param string $text
   *   The text that will be converted.
   * @param string $delimiter
   *   An optional string to separate each character.
   *
   * @return string
   *   The input $text with first letters of each word capitalized.
   */
  public static function initials($text, $delimiter = '') {
    // Convert the text to lowercase.
    $text = mb_strtolower($text);
    // Initialize an empty array to store the initials.
    $results = [];
    // Split the text into words and iterate over each word.
    foreach (array_filter(self::explode($text)) as $word) {
      // Add the first character of each word to the results array.
      $results[] = mb_substr($word, 0, 1);
    }
    // Join the initials with the specified delimiter.
    $text = implode($delimiter, $results);
    // Convert the initials to uppercase.
    $text = mb_strtoupper($text);
    // Return the initials, adding the delimiter if the text is not empty.
    return $text ? $text . $delimiter : '';
  }

}
