<?php

namespace Drupal\displayname\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\displayname\DisplayNameFormatParser;
use Drupal\displayname\Traits\DisplayNameAdditionalPreferredTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'display_name' formatter.
 *
 * @FieldFormatter(
 *   id = "display_name_default",
 *   module = "displayname",
 *   label = @Translation("Display name formatter"),
 *   field_types = {
 *     "display_name",
 *   }
 * )
 */
class DisplayNameFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  use DisplayNameAdditionalPreferredTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field renderer for any additional components.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The name format parser.
   *
   * @var \Drupal\displayname\DisplayNameFormatParser
   */
  protected $parser;

  /**
   * The logger channel for media.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a DisplayNameFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel for media.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The rendering service.
   * @param \Drupal\displayname\DisplayNameFormatParser $parser
   *   The name format parser.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LoggerInterface $logger, MessengerInterface $messenger, EntityFieldManager $entityFieldManager, EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer, DisplayNameFormatParser $parser) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory')->get('displayname'),
      $container->get('messenger'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('displayname.format_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings += [
      'format' => 'default',
      'markup' => 'none',
      'list_format' => '',
      'link_target' => '',
    ];
    $settings += self::getDefaultAdditionalPreferredSettings();
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Initialize the form elements with the parent settings form.
    $elements = parent::settingsForm($form, $form_state);

    // Add a select element for choosing the display name format.
    $elements['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Display name format'),
      '#default_value' => $this->getSetting('format'),
      '#options' => displayname_get_custom_format_options(),
      '#required' => TRUE,
    ];

    // Add a select element for choosing the list format.
    $elements['list_format'] = [
      '#type' => 'select',
      '#title' => $this->t('List format'),
      '#default_value' => $this->getSetting('list_format'),
      '#empty_option' => $this->t('-- individually --'),
      '#options' => displayname_get_custom_list_format_options(),
    ];

    // Add a select element for choosing the markup type.
    $elements['markup'] = [
      '#type' => 'select',
      '#title' => $this->t('Markup'),
      '#default_value' => $this->getSetting('markup'),
      '#options' => $this->parser->getMarkupOptions(),
      '#description' => $this->t('This option wraps the individual components of the name in SPAN elements with corresponding classes to the component.'),
      '#required' => TRUE,
    ];

    // If the field is associated with a specific bundle,
    // add a select element for choosing the link target.
    if (!empty($this->fieldDefinition->getTargetBundle())) {
      $elements['link_target'] = [
        '#type' => 'select',
        '#title' => $this->t('Link Target'),
        '#default_value' => $this->getSetting('link_target'),
        '#empty_option' => $this->t('-- no link --'),
        '#options' => $this->getLinkableTargets(),
      ];

      // Add additional settings for preferred and alternative names.
      $elements += $this->getNameAdditionalPreferredSettingsForm($form, $form_state);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    // Retrieve the current formatter settings.
    $settings = $this->getSettings();
    $summary = [];

    // Summary for the display name format setting.
    $machine_name = $settings['format'] ?? 'default';
    $formats = displayname_get_custom_format_options();
    $format = $formats[$machine_name] ?? 'Default';
    if ($format) {
      $summary[] = $this->t('Format: @format (@machine_name)', [
        '@format' => $format,
        '@machine_name' => $machine_name,
      ]);
    }
    else {
      $summary[] = $this->t('Format: <strong>Missing format.</strong><br/>This field will be displayed using the Default format.');
    }

    // Summary for the list format setting.
    if (!isset($settings['list_format']) || $settings['list_format'] == '') {
      $summary[] = $this->t('List format: Individually');
    }
    else {
      $machine_name = $settings['list_format'] ?? 'default';
      $formats = displayname_get_custom_list_format_options();
      $format = $formats[$machine_name] ?? 'Default';
      if ($format) {
        $summary[] = $this->t('List format: @format (@machine_name)', [
          '@format' => $format,
          '@machine_name' => $machine_name,
        ]);
      }
      else {
        $summary[] = $this->t('List format: <strong>Missing list format.</strong><br/>This field will be displayed using the Default list format.');
      }
    }

    // Summary for the markup type setting.
    $markup_options = $this->parser->getMarkupOptions();
    $summary[] = $this->t('Markup: @type', [
      '@type' => $markup_options[$this->getSetting('markup')],
    ]);

    // Summary for the link target setting.
    if (!empty($settings['link_target'])) {
      $targets = $this->getLinkableTargets();
      $summary[] = $this->t('Link: @target', [
        '@target' => empty($targets[$settings['link_target']]) ? $this->t('-- invalid --') : $targets[$settings['link_target']],
      ]);
    }

    // Add summaries for additional preferred settings.
    $this->settingsNameAdditionalPreferredSummary($summary);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Return early if there are no items to display.
    if (!$items->count()) {
      return $elements;
    }

    // Retrieve formatter settings.
    $settings = $this->settings;

    // Determine the format to use for displaying the name.
    $format = $settings['format'] ?? 'default';

    // Check if the field is set to handle multiple values.
    $is_multiple = $this->fieldDefinition->getFieldStorageDefinition()->isMultiple() && $items->count() > 1;

    // Determine the list format if the field has multiple values.
    $list_format = $is_multiple && !empty($settings['list_format']) ? $settings['list_format'] : '';

    // Parse any additional components like preferred or alternative names.
    $extra = $this->parseAdditionalComponents($items);

    // Get the URL to link to if 'link_target' is set in settings.
    $extra['url'] = empty($settings['link_target']) ? NULL : $this->getLinkableTargetUrl($items);

    $item_array = [];

    // Process each item in the FieldItemList.
    foreach ($items as $item) {
      // Combine the item's components with any additional components.
      $components = $item->toArray() + $extra;
      // Add the combined components to the item array.
      $item_array[] = $components;
    }

    // Format the items based on whether a list format is specified.
    if ($list_format) {
      // Format the list of items.
      $elements[0]['#markup'] = $this->formatter->formatList($item_array, $format, $list_format, $langcode);
    }
    else {
      // Format each item individually.
      foreach ($item_array as $delta => $item) {
        $elements[$delta]['#markup'] = $this->parser->parse($item, $format, $this->settings);
      }
    }

    return $elements;
  }

  /**
   * Determines with markup should be added to the results.
   *
   * @return bool
   *   Returns TRUE if markup should be applied.
   */
  protected function useMarkup() {
    return $this->settings['markup'];
  }

  /**
   * Find any linkable targets.
   *
   * @return array
   *   An array of possible targets.
   */
  protected function getLinkableTargets() {
    // Initialize the targets array with a default option for the entity URL.
    $targets = ['_self' => $this->t('Entity URL')];

    // Get the bundle and entity type ID for the field definition.
    $bundle = $this->fieldDefinition->getTargetBundle();
    $entity_type_id = $this->fieldDefinition->getTargetEntityTypeId();

    // Retrieve all field definitions for the entity type and bundle.
    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);

    // Loop through each field to identify potential linkable targets.
    foreach ($fields as $field) {
      // Check if the field is not a base field
      // and is of a type that can be linked.
      if (!$field->getFieldStorageDefinition()->isBaseField()) {
        switch ($field->getType()) {
          case 'entity_reference':
          case 'link':
            // Add the field to the targets array with its label.
            $targets[$field->getName()] = $field->getLabel();
            break;
        }
      }
    }

    return $targets;
  }

  /**
   * Gets the URL object.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The name formatter's FieldItemList.
   *
   * @return \Drupal\Core\Url
   *   Returns a Url object.
   */
  protected function getLinkableTargetUrl(FieldItemListInterface $items) {
    try {
      // Get the parent entity from the field items.
      $parent = $items->getEntity();

      // Check if the link target is the entity itself.
      if ($this->settings['link_target'] == '_self') {
        // Ensure the entity is not new and accessible.
        if (!$parent->isNew() && $parent->access('view')) {
          // Return the URL of the parent entity.
          return $parent->toUrl();
        }
      }
      // Check if the parent entity has the specified field for linking.
      elseif ($parent->hasField($this->settings['link_target'])) {
        // Get the target field items.
        $target_items = $parent->get($this->settings['link_target']);

        // Ensure the target field items are not empty.
        if (!$target_items->isEmpty()) {
          // Get the field definition of the target field.
          $field = $target_items->getFieldDefinition();

          // Handle different field types for creating URLs.
          switch ($field->getType()) {
            case 'entity_reference':
              // Loop through each item in the entity reference field.
              foreach ($target_items as $item) {
                // Ensure the referenced entity is not new and accessible.
                if (!empty($item->entity) && !$item->entity->isNew() && $item->entity->access('view')) {
                  // Return the URL of the referenced entity.
                  return $item->entity->toUrl();
                }
              }
              break;

            case 'link':
              // Loop through each item in the link field.
              foreach ($target_items as $item) {
                // Get and return the URL from the link field.
                if ($url = $item->getUrl()) {
                  return $url;
                }
              }
              break;
          }
        }
      }
    }
    catch (UndefinedLinkTemplateException $e) {
      // Log the exception to watchdog for debugging purposes.
      $this->logger->error('Undefined link template: @message', ['@message' => $e->getMessage()]);

      // Display a user-friendly error message.
      $this->messenger->addError($this->t('An error occurred while generating the link. Please contact the site administrator.'));
    }

    // Return a default route if no valid URL is found.
    return Url::fromRoute('<none>');
  }

  /**
   * Gets any additional linked components.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The name formatter's FieldItemList.
   *
   * @return array
   *   An array of any additional components if set.
   */
  protected function parseAdditionalComponents(FieldItemListInterface $items) {
    $extra = [];

    // Loop through the additional components we want to handle.
    foreach (['preferred', 'alternative'] as $key) {
      // Retrieve the field reference and separator settings for the component.
      $key_value = $this->getSetting($key . '_field_reference');
      $sep_value = $this->getSetting($key . '_field_reference_separator');

      // If the settings are not set, use the default field settings.
      if (!$key_value) {
        $key_value = $this->fieldDefinition->getSetting($key . '_field_reference');
        $sep_value = $this->fieldDefinition->getSetting($key . '_field_reference_separator');
      }

      // Get the additional component value.
      $value = displayname_get_additional_component($this->entityTypeManager, $this->renderer, $items, $key_value, $sep_value);

      // If a value is found, add it to the extra array.
      if ($value) {
        $extra[$key] = $value;
      }
    }

    return $extra;
  }

}
