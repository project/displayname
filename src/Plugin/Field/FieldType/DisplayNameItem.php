<?php

namespace Drupal\displayname\Plugin\Field\FieldType;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\displayname\Traits\DisplayNameAdditionalPreferredTrait;
use Drupal\displayname\Traits\DisplayNameFieldSettingsTrait;
use Drupal\displayname\Traits\DisplayNameFormDisplaySettingsTrait;
use Drupal\displayname\Traits\DisplayNameFormSettingsHelperTrait;

/**
 * Plugin implementation of the 'display_name' field type.
 *
 * Majority of the settings handling is delegated to the traits
 * so that these can be reused.
 *
 * @FieldType(
 *   id = "display_name",
 *   label = @Translation("Display name"),
 *   description = @Translation("Stores user full name or nickname."),
 *   default_widget = "display_name_default",
 *   default_formatter = "display_name_default"
 * )
 */
class DisplayNameItem extends FieldItemBase implements TrustedCallbackInterface {

  use DisplayNameFieldSettingsTrait;
  use DisplayNameFormDisplaySettingsTrait;
  use DisplayNameFormSettingsHelperTrait;
  use DisplayNameAdditionalPreferredTrait;

  /**
   * Definition of name field components.
   *
   * @var array
   */
  protected static $components = [
    'title',
    'first',
    'middle',
    'last',
    'full',
    'alias',
  ];

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [];
    foreach (static::$components as $key) {
      $columns[$key] = [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ];
    }
    return [
      'columns' => $columns,
      'indexes' => [
        'first' => ['first'],
        'last' => ['last'],
        'alias' => ['alias'],
        'full' => ['full'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = self::getDefaultDisplayNameFieldSettings();
    $settings += self::getDefaultDisplayNameFormDisplaySettings();
    $settings += self::getDefaultAdditionalPreferredSettings();
    $settings['override_format'] = 'default';
    return $settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    foreach (static::$components as $component) {
      $properties[$component] = DataDefinition::create('string')
        ->setLabel(new TranslatableMarkup('@component', ['@component' => ucfirst($component)]));
    }
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyDisplayName() {
    // There is no main property for this field item.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    foreach ($this->properties as $property) {
      $definition = $property->getDataDefinition();
      if (!$definition->isComputed() && $property->getValue() !== NULL) {
        return FALSE;
      }
    }
    if (isset($this->values)) {
      foreach ($this->values as $name => $value) {
        // Title & generational have no meaning by themselves.
        if ($name == 'title') {
          continue;
        }
        if (isset($value) && strlen($value) && !isset($this->properties[$name])) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    $active_components = $this->filteredArray();
    if (!$this->full && !in_array('full', $active_components)) {
      $full_name = '';
      foreach ($active_components as $component => $value) {
        if (!empty($value)) {
          $sep = empty($full_name) ? '' : ' ';
          if ($component == 'title' && $this->title != '-- --' && empty($full_name)) {
            $full_name .= $value;
          }
          elseif ($component == 'alias' && empty($full_name)) {
            $full_name .= ' (' . $value . ')';
          }
          else {
            $full_name .= $sep . $value;
          }
        }
      }
      if (!empty(trim($full_name))) {
        $this->full = trim($full_name);
      }
    }
  }

  /**
   * Returns active components only.
   *
   * @return array
   *   Array of filtered name component values.
   */
  public function filteredArray() {
    $values = [];
    $field = $this->getFieldDefinition();
    $settings = $field->getSettings();
    $active_components = array_filter($settings['components']);
    foreach ($this->getProperties() as $name => $property) {
      if (isset($active_components[$name]) && $active_components[$name]) {
        $values[$name] = $property->getValue();
      }
    }
    return $values;
  }

  /**
   * Get a list of active components.
   *
   * @return array
   *   Keyed array of active component labels.
   */
  public function activeComponents() {
    $settings = $this->getFieldDefinition()->getSettings();
    $components = [];
    foreach (_displayname_translations() as $key => $label) {
      if (!empty($settings['components'][$key])) {
        $components[$key] = empty($settings['labels'][$key]) ? $label : $settings['labels'][$key];
      }
    }

    return $components;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $element = $this->getDefaultDisplayNameFieldSettingsForm($settings, $form, $form_state);
    $element += $this->getDefaultDisplayNameFormDisplaySettingsForm($settings, $form, $form_state);
    foreach ($this->getNameAdditionalPreferredSettingsForm($form, $form_state) as $key => $value) {
      $element[$key] = $value;
      $element[$key]['#table_group'] = 'none';
      $element[$key]['#weight'] = 50;
    }

    $element['#pre_render'][] = [
      $this,
      'fieldSettingsFormPreRender',
    ];
    $element['#element_validate'] = [
      [
        get_class($this),
        'validateUserDisplayName',
      ],
    ];

    return $element;
  }

  /**
   * Manage whether the display name field should override a user's login name.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $complete_form
   *   The complete form structure.
   */
  public static function validateUserDisplayName(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $value = NULL;
    $config = \Drupal::configFactory()->getEditable('displayname.settings');

    // Ensure the display name field value should override a user's login name.
    if (!empty($element['user_display_name']) && $element['user_display_name']['#value'] == 1) {
      // Retrieve the display name field's machine name.
      $value = $element['field_display_name']['#default_value'];
    }

    // Ensure that the login-name-override configuration has changed.
    if ($config->get('user_display_name') != $value) {
      // Update the configuration with the new value.
      $config->set('user_display_name', $value)->save();

      // Retrieve the ID of all existing users.
      $query = \Drupal::entityQuery('user');
      $uids = $query->accessCheck()->execute();

      // Invalidate the cache for each user so that
      // the appropriate login name will be displayed.
      foreach ($uids as $uid) {
        Cache::invalidateTags(['user:' . $uid]);
      }

      \Drupal::logger('name')->notice('Cache cleared for data tagged as %tag.', ['%tag' => 'user:{$uid}']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    // Single reused generation of 100 random display names.
    $names = &drupal_static(__FUNCTION__, []);
    if (empty($names)) {
      $names = \Drupal::service('displayname.generator')->generateSampleDisplayNames(100, $field_definition);
    }
    return $names[array_rand($names)];
  }

}
