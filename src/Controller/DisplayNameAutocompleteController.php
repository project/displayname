<?php

namespace Drupal\displayname\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\displayname\DisplayNameAutocomplete;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for display name autocompletion routes.
 */
class DisplayNameAutocompleteController implements ContainerInjectionInterface {

  /**
   * The display name autocomplete helper class to find matching name values.
   *
   * @var \Drupal\displayname\DisplayNameAutocomplete
   */
  protected $displayNameAutocomplete;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a DisplayNameAutocompleteController object.
   *
   * @param \Drupal\displayname\DisplayNameAutocomplete $displayname_autocomplete
   *   The name autocomplete helper class to find matching name values.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(DisplayNameAutocomplete $displayname_autocomplete, EntityFieldManager $entityFieldManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->displayNameAutocomplete = $displayname_autocomplete;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('displayname.autocomplete'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns response for the name autocompletion.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object containing the search string.
   * @param string $field_name
   *   The field name.
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $component
   *   The name component.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the autocomplete suggestions.
   *
   * @see \Drupal\displayname\DisplayNameAutocomplete::getMatches()
   */
  public function autocomplete(Request $request, $field_name, $entity_type, $bundle, $component) {
    // Get the field definitions for the entity and bundle.
    $definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);

    // Check if the field exists in the definitions.
    if (!isset($definitions[$field_name])) {
      throw new AccessDeniedHttpException();
    }

    $field_definition = $definitions[$field_name];
    $access_control_handler = $this->entityTypeManager->getAccessControlHandler($entity_type);

    // Check if the field type is 'display_name'
    // and if the user has access to edit the field.
    if ($field_definition->getType() != 'display_name' || !$access_control_handler->fieldAccess('edit', $field_definition)) {
      throw new AccessDeniedHttpException();
    }

    // Get matching values for the autocomplete.
    $matches = $this->displayNameAutocomplete->getMatches($field_definition, $component, $request->query->get('q'));
    return new JsonResponse($matches);
  }

}
