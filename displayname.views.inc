<?php

/**
 * @file
 * Views integration for display name fields.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data().
 *
 * Adds new fulltext name filter handler for every display name field.
 */
function displayname_field_views_data(FieldStorageConfigInterface $field_storage) {
  // Get the default views data for the field.
  $data = views_field_default_views_data($field_storage);
  $field_name = $field_storage->getName();

  // Add a custom fulltext filter handler
  // for each table that contains the field.
  foreach ($data as $table_name => $table_data) {
    $data[$table_name][$field_name]['filter'] = [
      'field' => $field_name,
      'table' => $table_name,
      'field_name' => $field_name,
      'id' => 'display_name_fulltext',
      'allow_empty' => TRUE,
    ];
  }

  return $data;
}
